var mocha = require ('mocha');
var chai = require ('chai');
var chaihttp = require ('chai-http');

chai.use (chaihttp);

var should = chai.should ();

var server = require ('../server'); //ejecuta el server

describe ('first test suite',
    function () {
      it ('Test that DuckDuckGo works',
          function (done) {
            chai.request ('http://www.duckduckgo.com')
            .get ('/')
            .end (
              function (err,res) {
                console.log("Request has ended");
                console.log(err);
    //            console.log(res);
                res.should.have.status(200);
                done ();
              }
            )
          }
        )
    }
);


describe ('Test de API de usaurios Tech U',
    function () {
      it ('Prueba que la API  funciona correctamente',
          function (done) {
            chai.request ('http://localhost:3000')
            .get ('/apitechu/v1')
            .end (
              function (err,res) {
                console.log("Request has ended");
                res.should.have.status(200);
                res.body.msg.should.be.eql("Hola desde API TECHU");
                done ();
              }
            )
          }
        ),
        it ('Prueba que la API  devuelve una lista de usaurios correctos',
            function (done) {
              chai.request ('http://localhost:3000')
              .get ('/apitechu/v1/users')
              .end (
                function (err,res) {
                  console.log("Request has ended");
                  res.should.have.status(200);
                  res.body.should.be.a("array");

                  for (user of res.body) {
                    user.should.have.property('email');
                    user.should.have.property('password');
                  }
                  done ();
                }
              )
            }
          )
    }
);
