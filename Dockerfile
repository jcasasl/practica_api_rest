#Establece nuestro node
FROM node

#establece un directorio de trabajo donde se ejecutan los comandos
WORKDIR /apitechu

#Establece lo que vamos a añadir
ADD . /apitechu

#indica el puerto
EXPOSE 3000

#ejecutamos el comando para que empiece a funcionar.
CMD ["npm", "start"]
