var express = require('express');
var app = express();
var port = process.env.PORT || 3000;
var bodyParser =require('body-parser');
app.use(bodyParser.json());

//definimos variables para el consumo de la ApI de Mlab, para utilizarlo posteriormente
var baseMLabURL = "https://api.mlab.com/api/1/databases/techu/collections/";
var mLabAPIKey = "apiKey=1SJHBRHKdz44ZKQQ-yVbRlIkYahLW6PN";
var requestJson = require('request-json');

app.listen(port);
console.log ("API escuchando en el puerto " + port);

app.get(
  "/apitechu/v1", function(req, res) {
      console.log("get /apitechu/v1");
      res.send({"msg" : "Hola desde API TECHU"});
  }
);
app.get(
  "/apitechu/v1/users",
  function(req,res) {
    console.log("GET /apitechu/v1/users");
  //  res.sendFile('./Usuarios.json') --> Función deprecada
  res.sendFile('Usuarios.json', {root: __dirname});

  //var users = require('./Usuarios.json');
  //res.send(users);
  }
);

//crear un Usuarios
app.post("/apitechu/v1/users",
  function(req, res) {
    console.log ("POST /apitechu/v1/users");
    //<console.log(req.headers);
   console.log("first_name is" + req.body.first_name);
   console.log("last_name is" + req.body.last_name);
   console.log("country is" + req.body.country);

    var newUser = {
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "country" : req.body.country
    };

    var users = require('./Usuarios.json');
    users.push(newUser);

    writeUserDataToFile(users);

    console.log("usaurio guardado con exito");
    res.send({"msg" : "usuario guardado con exito"});

  }
)

app.delete("/apitechu/v1/users/:id",
  function(req, res){
    console.log("delete /apitechu/v1/users/:id");
    console.log(req.params);
    console.log(req.params.id);

    var users = require('./Usuarios.json');
    //función splice que coge el array, quita un trozo y devuelve el array sin eltrozo.
    users.splice(req.params.id -1, 1);
    writeUserDataToFile(users);
    console.log("usuario borrado");
    res.send({"msg" : "Usuario borrado"});

  }
)

//dado que una vez que se borra, hay que escribir el fichero se saca la parte de la escritura del delete
// y del put a una función

function writeUserDataToFile(data) {

  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);

  fs.writeFile("./Users_pwd.json", jsonUserData, "utf8",
     function(err) {
       if(err) {
         console.log(err);
       } else {
         console.log("Archivo salvado");
       }
     }
  );
}


//función POST utilizando todos los tipos de parametro

app.post("/apitechu/v1/monstruo/:p1/:p2",
  function (req, res) {
    console.log("Parametros");
    console.log(req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("Body");
    console.log(req.body);

    console.log("Headers");
    console.log(req.headers);

  }
);

app.post("/apitechu/v1/login",
function(req, res) {
  console.log ("POST /apitechu/v1/login");
  var newUser = {
    "email" : req.body.email,
    "password" : req.body.password
  }

    console.log("email is" + req.body.email);
    console.log("password is" + req.body.password);

  var users = require("./Users_pwd.json");
  var mensaje = "login incorrecto";
  var encontrado = false;

  for (user of users) {
    if (user.email == newUser.email && user.password == newUser.password) {
      console.log("usuario encontrado" + user.email);
      encontrado = true;
      mensaje = "Login correcto";
      user.logged=true;
      writeUserDataToFile(users);
    } else if (user.email == newUser.email && user.password != newUser.password){
      console.log("passowrd incorrecta" + user.password);
      encontrado = true;
    }

  }
  if (!encontrado) {
    mensaje = "Usuario / password incorrecta" ;
  }

  res.send (mensaje);
}

);

app.post("/apitechu/v1/logout",
  function(req, res) {
    console.log ("POST /apitechu/v1/logout");
    var newUser = {
      "id" : req.body.id
    }

    console.log("id_usuario is " + req.body.id);


    var users = require("./Users_pwd.json");
    var mensaje = "logout incorrecto";
    var encontrado = false;

    for (user of users) {
      if (user.id == newUser.id && user.logged) {
        console.log("usuario encontrado" + user.first_name);
        encontrado = true;
        mensaje = "Logout correcto";
        delete user.logged;
        writeUserDataToFile(users);
      } else if (user.id == newUser.id){
        console.log("Usuario no logado " + user.first_name);
        mensaje = "Usuario no logado";
        encontrado = true;
      }

    }
    if (!encontrado) {
      mensaje = "Usuario inexistente" ;
    }

    res.send (mensaje);
  }
)

// creamos una nueva función para obtener los datos desde mLab
app.get(
  "/apitechu/v2/users",
  function(req,res) {
    console.log("GET /apitechu/v2/users");

//creamos el cliente
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("cliente http creado");

//hacemos las peticiones a partir de la base, y añadiendo los filtros y el apiKey
    httpClient.get("user?"+ mLabAPIKey,
       function (err, resMLab, body) {
         var response = !err ? body : {
           "msg" : "Error obteniendo usuarios"
         }
         res.send (response);
       }
    )
  }
);


// creamos una nueva por Id
app.get(
  "/apitechu/v2/users/:id",
  function(req,res) {
    console.log("GET /apitechu/v2/users/:id");

    var id = req.params.id;
    var query = 'q={"id":' + id + '}';

    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("cliente http creado");

    httpClient.get("user?"+ query + "&" + mLabAPIKey,
       function (err, resMLab, body) {
//         var response = !err ? body : {
//           "msg" : "Error obteniendo usuario."
//         }

            var response = {};
            if (err) {
              response = {
                 "msg" : "Error obteniendo usuario."
              }
              res.status(500);
            } else {
                if (body.length > 0) {
                  response = body;
                } else {
                  response = {
                    "msg" : "Usuario no encontrado"
                  };
                  res.status(404);
                }
            }
         res.send (response);
       }
    )
  }
);

//Login y Logout utilizando en mLab Versión 2 de los métodos.
app.post("/apitechu/v2/login",
function(req, res) {
  console.log ("POST /apitechu/v2/login");

    var newUser = {
      "email" : req.body.email,
      "password" : req.body.password
    }

      console.log("email is" + req.body.email);
      console.log("password is" + req.body.password);

//    var query = 'q={"email": "' + newUser.email + '", "password" : "' + newUser.password + '"}';
    var query = 'q={"email": "' + newUser.email + '"}';
    console.log ("query -->" + query);

    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("cliente para login http creado");

    httpClient.get("user?"+ query + "&" + mLabAPIKey,
       function (err, resMLab, body) {

            var response = {};
//            var mensaje = "login incorrecto";
            var encontrado = false;
            var putBody = '{"$set":{"logged":true}}';

            if (err) {
                response = {
                   "msg" : "Error obteniendo usuario."
                }
                res.status(500);
          } else {
                if (body.length > 0) {
                  response = body[0];
                  console.log ("response -->" + response);

                  if (response["password"] == newUser.password) {
                       console.log("usuario encontrado-->" + response["password"]);
                       encontrado = true;

                  }  else {
                       console.log("passowrd incorrecta" + newUser.password);
                       response = {
                                  "msg" : "Password Incorrecta"
                        };
                        res.status(404);
                  }
                } else {
                        response = {
                                   "msg" : "Usuario no encontrado"
                         };
                         res.status(404);
                }
            }
         console.log ("encontrado --> " + encontrado);
         if (encontrado) {
           httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
             function (err,resMLab,body) {
               if (err) {
                   response = {
                      "msg" : "Error haciendo login usuario."
                   }
                   res.status(500);
              } else {
                   console.log ("login realizado ");
                   response = {
                     "msg" : "Login realizado correctamente.",
                     "id" : response["id"]
                   }
              }
              res.send (response);
             }
           );
         }
//         res.send (response);

     }
   );
   }
);


//Logout en Mongo con mLab

app.post ("/apitechu/v2/logout",
  function(req,res){
      console.log("POST /apitechu/v2/logout");
      var id = req.body.id;
      var query = 'q={"id":' + id + ',"logged": true}';
      var response;
      var httpClient = requestJson.createClient(baseMLabURL);

      httpClient.get("user?" +  query + "&" +  mLabAPIKey,
        function(err, resMLab, body){
          if (err)
          {
            response = {
                "msg" : "Error obteniendo usuario"
            };
            res.status(500);
            res.send(response);
          } else {
            if (body.length <=0){
              response = {
                "msg" : "logout incorrecto"
              };
              res.status(404);
              res.send(response);
            }else{
              var putBody = '{"$unset":{"logged":""}}';
              httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),function(err, resPutMlab, body){
                if (err)
                {
                  response = {
                      "msg" : "Error marcando usuario logado"
                  };
                  res.status(500);
                } else {
                  console.log ("logout correcto");
                  response = {
                      "msg" : "logout correcto"
                  };
                }
                res.send(response);
              });
            }
          }
        }
      );
    }
);

// creamos una funcion para obtener cuentas por Id
app.get(
  "/apitechu/v2/users/:id/accounts",
  function(req,res) {
    console.log("GET /apitechu/v2/users/:id/accounts");

    var id = req.params.id;
    var query = 'q={"userid":' + id + '}';

    console.log("Id-->" + id);

    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("cliente http creado");

    httpClient.get("account?"+ query + "&" + mLabAPIKey,
       function (err, resMLab, body) {
//         var response = !err ? body : {
//           "msg" : "Error obteniendo usuario."
//         }

            var response = {};
            if (err) {
              response = {
                 "msg" : "Error obteniendo usuario."
              }
              res.status(500);
            } else {
                if (body.length > 0) {
                  response = body; // body[0] solo devolvemos 1 de memento esto se podría sustituir poniendo en la query &l=1
                } else {
                  response = {
                    "msg" : "No existen datos para la consulta"
                  };
                  res.status(404);
                }
            }
         res.send (response);
       }
    )
  }
)
